# Laboratory nr.3 - Linear Regression
#### Table of contents
* [Introduction](#introduction)
* [Modeling types](#modeling-types)
* [Technologies](#technologies)
* [Setup](#setup)
* [Resources](#resources)

## Introduction
The main task of the laboratory work nr.3 is to predict the price of an apartment given a set of characteristics of the apartment complex it is part of.
Also, for determining the accuracy of the model some statistic tests are provided.

## Modeling types
The current program predicts the price by:
* Multi linear regression
* Keras neuronal networks

## Technologies
Used technologies:
* Python
* Google Colab Notebook
* Pandas module
* Scikit-learn module
* Numpy module
* Matplotlib
* Seaborn module

## Setup
In order to run the project:
1. Open Google Colab Notebook
2. Choose 'Open Notebook'
3. Upload .ipynb file
4. Upload the apartment data
5. Run

## Resources
UTM Fundamentals of Artificial Intelligence Course
